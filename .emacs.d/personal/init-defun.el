;; Some personal functions

(defun my-read-only-mode ()
  ;; Only if buffer is not empty
  (if (and (> (buffer-size) 0)
           ;; and not if buffer is associated with some file
           ;; inside user's emacs directory.
           (not (file-in-directory-p
                 (or buffer-file-name "")
                 user-emacs-directory)))
      (read-only-mode 1)))

(defun current-line-empty-p ()
  ;; Is the line at (point) empty or only having whitespace(s)?
  (= 0
   (string-match-p "^\\s-*$" (thing-at-point 'line))))

(defun next-line-empty-p ()
  (save-excursion
    (forward-line 1)
    ;; end of buffer (file) reached?
    (if (eobp)
        nil
      (current-line-empty-p))))

(defun force-save-buffer ()
  "Disable `read-only-mode' if enabled and try to save the buffer.
Re-enable `read-only-mode' again if it was before saving the buffer."
  (interactive)
  (let ((read-only-enabled buffer-read-only))
    (read-only-mode -1)
    (save-buffer)
    (if read-only-enabled
        (read-only-mode 1))))

(defun kill-region-or-backward-word ()
  "If the region is active and non-empty, call `kill-region'.
Otherwise, call `backward-kill-word'."
  (interactive)
  (call-interactively
   ;; Act on currently marked region, if any
   (if (use-region-p)
       'kill-region
     'backward-kill-word)))

(defun my-zap-to-char (arg char)
  "zap-to-char version for non-interactive use,
won't modify kill ring."
  (delete-region
   (point)
   (progn
     (search-forward (char-to-string char) nil nil arg)
     (point))))

(defun my-git-add ()
  (interactive)
  (call-process "git" nil nil nil "add" (buffer-file-name))
  ;; FIXME: There isn't any easy way to ensure if file is really added
  (message "File Added"))

(defun my-term ()
  (interactive)
  ;; if already on term, switch to previously opened buffer
  (cond ((eq major-mode 'term-mode)
         (previous-buffer))
        ;; else if a term is already open, switch to that
        ((get-buffer "*ansi-term*")
         (switch-to-buffer "*ansi-term*"))
        ;; else create a new term buffer
        (t
         (ansi-term (getenv "SHELL")))))

(defun kill-buffer-on-exit (process event)
  (when (memq (process-status process) '(exit signal))
    (kill-buffer)))

(defun kill-process-buffer-on-exit ()
  (set-process-sentinel (get-buffer-process (current-buffer))
                        #'kill-buffer-on-exit))

(provide 'init-defun)
