# -*- mode: snippet -*-
# name: case get_property
# key: c.g
# expand-env: ((yas-after-exit-snippet-hook (lambda () (indent-region yas-snippet-beg yas-snippet-end) (read-only-mode))))
# --
case PROP_${1:$$(upcase yas-text)}:
  g_value_set_$2 (value, priv->${1:$(downcase yas-text)});
  break;`(unless (next-line-empty-p) "\n")`