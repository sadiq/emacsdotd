# -*- mode: snippet -*-
# name: case set_property
# key: c.s
# expand-env: ((yas-after-exit-snippet-hook (lambda () (indent-region yas-snippet-beg yas-snippet-end) (read-only-mode))))
# --
case PROP_${1:$$(upcase yas-text)}:
      ${3:`(str-to-style
                     (file-name-sans-extension
                     (file-name-nondirectory buffer-file-name))
                     "snake")`}_set_${1:$(downcase
                     yas-text)} (self, g_value_get_$2 (value));
                     break;`(unless (next-line-empty-p)
                     "\n")`